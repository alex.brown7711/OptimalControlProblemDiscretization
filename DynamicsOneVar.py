#!/usr/bin/python

# Standard Lib
import matplotlib.pyplot as plt
import numpy as np
import sys

from scipy.integrate import solve_ivp

# Include In Path
# sys.path.append("")

##===============================================================================
#
def main():
    # Time
    t0 = 0.0  # Initial Time
    dt = 0.1  # Step size
    tf = 10.0 # Final Time

    # Initial state
    x0 = 0

    # Control input function
    u = 1

    # Create Plots
    fig, (ax1, ax2, ax3) = plt.subplots(3,2)

    # Solve via pyODEIntegration
    tvec, xvec_ode = pyODEIntegration(x0, t0, dt, tf, u, chargerDynamics)
    
    # Plot pyODEIntegration
    ax1[0].plot(tvec, xvec_ode[0], '.-')
    ax1[0].set_ylabel('ODE')

    # Solve via Euler Integration
    tvec, xvec_euler = eulerIntegration(x0, t0, dt, tf, u, chargerDynamics)

    #  Plot Euler Intragraion
    ax2[0].plot(tvec, xvec_euler, '.-')
    ax2[0].set_ylabel('Euler')
    
    # Solve via RK4 Intragration
    tvec, xvec_rk4 = rk4Integration(x0, t0, dt, tf, u, chargerDynamics)
    
    # Plot RK4 Intragraion
    ax3[0].plot(tvec, xvec_rk4, '.-')
    ax3[0].set_ylabel('RK4')

    # Plot Euler Error
    euler_err = np.absolute(np.subtract(xvec_ode[0], xvec_euler))
    ax2[1].plot(tvec, euler_err, '.-')
    ax2[1].set_ylabel('Euler Error')

    # Plot RK4 Error  
    rk4_err = np.absolute(np.subtract(xvec_ode[0], xvec_rk4))
    ax3[1].plot(tvec, rk4_err, '.-')
    ax3[1].set_ylabel('RK4 Error')

    # Display plot
    plt.show()

    return

##===============================================================================
#
def chargerDynamics(t, x, u): 
    a = -0.5
    M = 95

    return a*x - a*M

##===============================================================================
# Basic Euler integration for functions of one variable.
# TODO: Generalize to do arbitrary amount of variables
# TODO: Allow control to be non-constant
def eulerIntegration(x0, t0, dt, tf, u, f): 
    tvec    = np.arange(t0, tf, dt)
    steps   = tvec.size
    xvec    = [0] * steps
    xvec[0] = x0
    x       = x0

    for k in range(1, steps):
        t = tvec[k-1]
        xdot = f(t, x, u)
        
        x = x + dt * xdot
        xvec[k] = x

    return tvec, xvec

##===============================================================================
# Basic RK4 Implementation for functions of one variable
# TODO: Generalize to do arbitary amount of variables
# TODO: Allow control to be non-constant
def rk4Integration(x0, t0, dt, tf, u, f):
    tvec    = np.arange(t0, tf, dt)
    steps   = tvec.size
    xvec    = [0] * steps
    xvec[0] = x0
    x       = x0

    for k in range(1, steps):
        t = tvec[k-1]
        
        k1 = f(t      , x           , u)
        k2 = f(t+dt/2 , x+(dt/2)*k1 , u)
        k3 = f(t+dt/2 , x+(dt/2)*k2 , u)
        k4 = f(t+dt/2 , x+(dt*k3)   , u)
        I  = k1*(dt/6) + k2*(dt/3) + k3*(dt/3) + k4*(dt/6)

        x  = x + I

        xvec[k] = x

    return tvec, xvec

##===============================================================================
#
def pyODEIntegration(x0, t0, dt, tf, u, f):
    tvec = np.arange(t0, tf, dt)
    sol  = solve_ivp(f, [t0, tf], [x0], method='RK23', args=(u,), dense_output=True, first_step=dt)

    return tvec, sol.sol(tvec)

##===============================================================================
#
if __name__ == "__main__":
    main()
