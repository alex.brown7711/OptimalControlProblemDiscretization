# Optimal Control Project Discretization
This is the code to generate the discretized dynamics plots for the Optimal Control project.

# Python code

## Running
```
python DynamicsOneVar.py
```

## Dependencies

* numpy
* matplotlib

